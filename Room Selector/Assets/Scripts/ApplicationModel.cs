﻿using System;
using UnityEngine;

namespace RoomSelector
{
    public class ApplicationModel : MonoBehaviour
    {
        #pragma warning disable 649
        [SerializeField] private Sprite _boyBackground;
        [SerializeField] private Sprite _girlBackground;
        #pragma warning restore 649
        public const string BoyRoomText = "BOY ROOM";
        public const string GirlRoomText = "GIRL ROOM";

        public const int LobbyScene = 0;
        public const int RoomScene = 1;

        public static ApplicationModel Instance { get; private set; }

        private void Awake()
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }

        public Sprite GetBackground(RoomType roomType)
        {
            switch (roomType)
            {
                case RoomType.BoyRoom:
                    return _boyBackground;
                case RoomType.GirlRoom:
                    return _girlBackground;
                default:
                    throw new ArgumentOutOfRangeException("roomType", roomType, null);
            }
        }
    }
}