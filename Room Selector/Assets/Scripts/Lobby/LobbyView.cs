﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace RoomSelector.Lobby
{
    public class LobbyView : View
    {
        #region Fields
        #pragma warning disable 649
        [SerializeField] private Button _boyButton;
        [SerializeField] private Button _girlButton;
        [SerializeField] private Button _creditsButton;
        [SerializeField] private MessageBoxView _messageBox;
        #pragma warning restore 649
        #endregion

        #region Events
        public event Action<RoomType> RoomSelected = t => { };
        #endregion

        #region Unity methods
        protected override void Awake()
        {
            SetEventHandlers();
            base.Awake();
        }

        private void OnDestroy()
        {
            _boyButton.onClick.RemoveListener(OnBoyButtonClick);
            _girlButton.onClick.RemoveListener(OnGirlButtonClick);
            _creditsButton.onClick.RemoveListener(OnCreditsButtonClick);
        }
        #endregion

        #region Event implementation
        private void OnBoyButtonClick()
        {
            RoomSelected(RoomType.BoyRoom);
        }

        private void OnGirlButtonClick()
        {
            RoomSelected(RoomType.GirlRoom);
        }

        private void OnCreditsButtonClick()
        {
            _messageBox.Show();
        }
        #endregion

        private void SetEventHandlers()
        {
            _boyButton.onClick.AddListener(OnBoyButtonClick);
            _girlButton.onClick.AddListener(OnGirlButtonClick);
            _creditsButton.onClick.AddListener(OnCreditsButtonClick);
        }
    }
}
