﻿using UnityEngine;
using UnityEngine.UI;

namespace RoomSelector.Lobby
{
    public class MessageBoxView : MonoBehaviour
    {
        #pragma warning disable 649
        [SerializeField] private Button _okButton;
        [SerializeField] private Button _backButton;
        #pragma warning restore 649

        private void Awake()
        {
            Hide();
            _okButton.onClick.RemoveAllListeners();
            _okButton.onClick.AddListener(Hide);
            _backButton.onClick.RemoveAllListeners();
            _backButton.onClick.AddListener(Hide);
        }
    
        public void Show()
        {
            gameObject.SetActive(true);
        }

        private void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}
