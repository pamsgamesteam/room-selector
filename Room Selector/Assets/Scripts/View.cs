﻿using UnityEngine;

namespace RoomSelector
{
    public class View : MonoBehaviour
    {
        protected virtual void Awake()
        {
            Controller.Instance.ViewLoaded(this);
        }
    }
}

