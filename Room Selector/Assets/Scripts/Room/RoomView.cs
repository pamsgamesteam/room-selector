﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace RoomSelector.Room
{
    public class RoomView : View
    {
        #region Fields
        #pragma warning disable 649
        [SerializeField] private Image _background;
        [SerializeField] private Text _text;
        [SerializeField] private Button _backButton;
#pragma warning restore 649
        #endregion

        public event Action BackButtonClicked=() => { };
        
        #region Unity methods
        protected override void Awake()
        {
            _backButton.onClick.AddListener(OnBackButtonClick);
            base.Awake();
        }

        private void OnDestroy()
        {
            _backButton.onClick.RemoveListener(OnBackButtonClick);

        }
        #endregion

        private void OnBackButtonClick()
        {
            BackButtonClicked();
        }

        public void SetParams(Sprite background, string text)
        {
            _background.sprite = background;
            _text.text = text;
        }
    }
}
