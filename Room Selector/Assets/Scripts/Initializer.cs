﻿using UnityEngine;

namespace RoomSelector
{
    public class Initializer : MonoBehaviour
    {
        [SerializeField]
#pragma warning disable 649
        private ApplicationModel _applicationModelPrefab;
#pragma warning restore 649

        private void Awake()
        {
            if (ApplicationModel.Instance == null)
                Instantiate(_applicationModelPrefab);
        }

    }
}
