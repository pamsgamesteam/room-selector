﻿using System;
using RoomSelector.Lobby;
using RoomSelector.Room;
using UnityEngine.SceneManagement;

namespace RoomSelector
{
    public class Controller
    {
        #region Singletone
        private static Controller _instance;
        public static Controller Instance {get { return _instance ?? (_instance = new Controller()); } }
        #endregion
        
        #region Fields
        private LobbyView _lobbyView;
        private RoomView _roomView;
        private RoomType _roomType;
        #endregion

        public void ViewLoaded(View view)
        {
            if (view is LobbyView)
            {
                RemoveLobbyEventHandlers();
                _lobbyView = (LobbyView)view;
                SetLobbyEventHandlers();
            }
            else if (view is RoomView)
            {
                RemoveRoomEventHandlers();
                _roomView = (RoomView)view;
                SetRoomEventHandlers();
                string text;
                switch (_roomType)
                {
                    case RoomType.BoyRoom:
                        text = ApplicationModel.BoyRoomText;
                        break;
                    case RoomType.GirlRoom:
                        text = ApplicationModel.GirlRoomText;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
                _roomView.SetParams(ApplicationModel.Instance.GetBackground(_roomType), text);
            }
        }

        private void SetLobbyEventHandlers()
        {
            _lobbyView.RoomSelected+=LobbyViewOnRoomSelected;
        }

        private void RemoveLobbyEventHandlers()
        {
            if(_lobbyView==null)return;
            _lobbyView.RoomSelected -= LobbyViewOnRoomSelected;
        }

        private void SetRoomEventHandlers()
        {
            _roomView.BackButtonClicked+=RoomViewOnBackButtonClicked;
        }

        private void RemoveRoomEventHandlers()
        {
            if(_roomView==null)return;
            
            _roomView.BackButtonClicked-=RoomViewOnBackButtonClicked;

        }

        #region Event handlers
        private void LobbyViewOnRoomSelected(RoomType roomType)
        {
            _roomType = roomType;
            SceneManager.LoadScene(ApplicationModel.RoomScene);
        }

        private void RoomViewOnBackButtonClicked()
        {
            SceneManager.LoadScene(ApplicationModel.LobbyScene);
        }
        #endregion
    }
}
